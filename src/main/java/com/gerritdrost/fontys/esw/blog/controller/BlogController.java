package com.gerritdrost.fontys.esw.blog.controller;

import com.gerritdrost.fontys.esw.blog.model.Comment;
import com.gerritdrost.fontys.esw.blog.model.Post;
import com.gerritdrost.fontys.esw.blog.service.BlogService;
import org.jtwig.web.servlet.JtwigRenderer;

import javax.json.*;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.List;

@WebServlet("/")
public class BlogController extends HttpServlet {

    private final JtwigRenderer twig = JtwigRenderer.defaultRenderer();
    private final BlogService blogService = new BlogService();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        SessionWrapper sessionWrapper = new SessionWrapper(request);

        switch (request.getServletPath()) {
            case "/visitor":
                sessionWrapper.setUserType("visitor");
                break;

            case "/admin-basic":
                sessionWrapper.setUserType("admin-basic");
                break;

            case "/admin-advanced":
                sessionWrapper.setUserType("admin-advanced");
                break;

            case "/posts":
                List<Post> posts = blogService.getPosts();

                response.setContentType("application/json");

                Json
                    .createWriter(response.getOutputStream())
                    .write(JsonMapper.postsToArray(posts));

                break;

            default:
                // Tell the client we're serving html
                response.setContentType("text/html");

                twig.dispatcherFor("/index.html.twig")
                        .with("title", "Blog")
                        .with("userType", sessionWrapper.getUserType())
                        .render(request, response);
        }
    }



    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        switch (request.getServletPath()) {
            case "/post": {
                String title = request.getParameter("title");
                String content = request.getParameter("content");

                Post post = blogService.createPost(title, content, new Date());

                response.setContentType("application/json");

                Json
                    .createWriter(response.getOutputStream())
                    .write(JsonMapper.postToObject(post));
            }
            break;
            case "/comment": {
                long postId = Long.parseLong(request.getParameter("post-id"));
                String content = request.getParameter("content");

                Comment comment = blogService.createComment(postId, content, new Date());

                response.setContentType("application/json");

                Json
                    .createWriter(response.getOutputStream())
                    .write(JsonMapper.commentToObject(comment));
            }
            break;
        }
    }

}
