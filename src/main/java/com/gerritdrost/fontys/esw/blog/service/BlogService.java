package com.gerritdrost.fontys.esw.blog.service;

import com.gerritdrost.fontys.esw.blog.dao.PostStorage;
import com.gerritdrost.fontys.esw.blog.dao.InMemoryPostStorage;

import java.util.Date;
import java.util.List;

import com.gerritdrost.fontys.esw.blog.model.Comment;
import com.gerritdrost.fontys.esw.blog.model.Post;

public class BlogService {

    private PostStorage postStorage;

    public BlogService() {
        // For now we use an in-memory storage for posts
        postStorage = new InMemoryPostStorage();
    }

    public Post createPost(String title, String content, Date date) {
        return postStorage.create(title, content, date);
    }

    public Post updatePost(Post p) {
        return postStorage.update(p);
    }

    public Comment createComment(long postId, String content, Date date) {
        Post post = getPost(postId);

        Comment comment = post.createComment(content, date);

        updatePost(post);

        return comment;
    }

    public Post getPost(long postId) {
        return postStorage.find(postId);
    }

    public List<Post> getPosts() {
        return postStorage.findAll();
    }
}
