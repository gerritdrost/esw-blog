/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.gerritdrost.fontys.esw.blog.model;

import java.util.Date;

public class Comment {
    private long postId;
    private long id;
    private String content;
    private Date date;

    public Comment(long postId, long id, String content) {
        this(postId, id, content, new Date());
    }

    public Comment(long postId, long id, String content, Date date) {
        this.postId = postId;
        this.id = id;
        this.content = content;
        this.date = date;
    }

    public Long getId() {
        return id;
    }

    public long getPostId() {
        return postId;
    }

    public Date getDate() {
        return date;
    }

    public String getContent() {
        return content;
    }

    public Comment withId(long commentId) {
        return new Comment(postId, commentId, content, date);
    }

}
