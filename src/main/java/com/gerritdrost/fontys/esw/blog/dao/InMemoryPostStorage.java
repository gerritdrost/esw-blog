/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gerritdrost.fontys.esw.blog.dao;

import java.util.*;

import com.gerritdrost.fontys.esw.blog.model.Post;

/**
 *
 * @author Administrator
 */
public class InMemoryPostStorage implements PostStorage {

    private long nextId;
    private HashMap<Long, Post> posts;

    public InMemoryPostStorage() {
        posts = new HashMap<>();
    }

    @Override
    public Post update(Post post) {
        if (post == null) {
            throw new IllegalArgumentException("Post is null");
        }

        if (!posts.containsKey(post.getId())) {
            throw new IllegalArgumentException("Provided post should be known, but id can't be found");
        }

        posts.put(post.getId(), post);

        return post;
    }

    @Override
    public Post create(String title, String content, Date date) {
        Post post = new Post(nextId++, title, content);

        posts.put(post.getId(), post);

        return post;
    }

    @Override
    public List<Post> findAll() {
        return new ArrayList<>(posts.values());
    }

    @Override
    public Post find(long id) {
        if (!posts.containsKey(id)) {
            throw new IllegalArgumentException("Id not found" + id);
        }

        return posts.get(id);
    }
}
