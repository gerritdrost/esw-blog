/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gerritdrost.fontys.esw.blog.dao;

import java.util.Date;
import java.util.List;

import com.gerritdrost.fontys.esw.blog.model.Post;

public interface PostStorage {

    Post create(String title, String content, Date date);
    Post update(Post p);

    List<Post> findAll();

    Post find(long id);

}
