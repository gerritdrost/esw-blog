package com.gerritdrost.fontys.esw.blog.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class SessionWrapper {
    private String userType;
    private HttpSession httpSession;

    public SessionWrapper(HttpServletRequest request) {
        httpSession = request.getSession(true);

        userType = (String) httpSession.getAttribute("user-type");

        if (userType == null) {
            setUserType("admin-advanced");
        }
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String value) {
        userType = value;
        httpSession.setAttribute("user-type", value);
    }
}
