/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gerritdrost.fontys.esw.blog.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Post {
    private long id;
    private String title;
    private String content;
    private Date date;
    private List<Comment> comments;

    private long commentId;


    public Post(String title, String content) {
        this(-1L, title, content);
    }

    public Post(String title, String content, List<Comment> comments) {
        this(-1L, title, content, new Date(), comments);
    }

    public Post(String title, String content, Date date) {
        this(-1L, title, content, date, new ArrayList<>());
    }

    public Post(String title, String content, Date date, List<Comment> comments) {
        this(-1L, title, content, date, comments);
    }

    public Post(long id, String title, String content) {
        this(id, title, content, new Date());
    }

    public Post(long id, String title, String content, List<Comment> comments) {
        this(id, title, content, new Date(), comments);
    }

    public Post(long id, String title, String content, Date date) {
        this(id, title, content, date, new ArrayList<>());
    }

    public Post(long id, String title, String content, Date date, List<Comment> comments) {
        this.id = id;
        this.date = date;
        this.title = title;
        this.content = content;
        this.comments = new ArrayList<>();

        this.commentId = 0L;

        for (Comment comment : comments) {
            addComment(comment);
        }
    }

    public long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }

    public Date getDate() {
        return date;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public Comment addComment(Comment comment) {
        Comment addComment = comment.withId(commentId++);

        comments.add(addComment);

        return addComment;
    }

    public Comment createComment(String content, Date date) {
        Comment comment = new Comment(id, commentId++, content, date);

        comments.add(comment);

        return comment;
    }
}
