package com.gerritdrost.fontys.esw.blog.controller;

import com.gerritdrost.fontys.esw.blog.model.Comment;
import com.gerritdrost.fontys.esw.blog.model.Post;

import javax.json.*;
import java.util.List;

public class JsonMapper {
    public static JsonObjectBuilder postToObjectBuilder(Post post) {
        return
                Json.createObjectBuilder()
                        .add("id", post.getId())
                        .add("title", post.getTitle())
                        .add("content", post.getContent())
                        .add("comments", commentsToArrayBuilder(post.getComments()));
    }

    public static JsonObject postToObject(Post post) {
        return
                postToObjectBuilder(post)
                        .build();
    }

    public static JsonArray commentsToArray(List<Comment> comments) {
        return commentsToArrayBuilder(comments)
                .build();
    }

    public static JsonArrayBuilder commentsToArrayBuilder(List<Comment> comments) {
        JsonArrayBuilder builder = Json.createArrayBuilder();

        for (Comment comment : comments) {
            builder.add(commentToObjectBuilder(comment));
        }

        return builder;
    }

    public static JsonArray postsToArray(List<Post> posts) {
        return postsToArrayBuilder(posts)
                .build();
    }

    public static JsonArrayBuilder postsToArrayBuilder(List<Post> posts) {
        JsonArrayBuilder builder = Json.createArrayBuilder();

        for (Post post : posts) {
            builder.add(postToObjectBuilder(post));
        }

        return builder;
    }

    public static JsonObject commentToObject(Comment comment) {
        return
                commentToObjectBuilder(comment)
                        .build();
    }

    public static JsonObjectBuilder commentToObjectBuilder(Comment comment) {
        return
                Json.createObjectBuilder()
                        .add("id", comment.getId())
                        .add("postId", comment.getPostId())
                        .add("content", comment.getContent());
    }
}
